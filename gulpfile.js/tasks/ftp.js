// ==== FTP ==== //

var gulp        = require('gulp')
  , plugins     = require('gulp-load-plugins')({ camelize: true })
  , config      = require('../../gulpconfig').images
  , ftp         = require('vinyl-ftp')
;

/* ***** Deployment ***** */
var ftpconfig = {
    host:     'ftp.asyluminfo.be',
    ftpname: 'hotbelgo',
    ftppword: 'sp6Am0_5',
    beta: '/httpdocs/wp-content/themes/asyluminfo/',
    production: '/public_html/xxxxxxxxxxxxx/'
};

gulp.task('ftp', function () {
    var conn = ftp.create( {
        host:     ftpconfig['host'],
        user:     ftpconfig['ftpname'],
        password: ftpconfig['ftppword'],
        parallel: 5
    } );

    var ftpDir = ftpconfig.beta;

    return gulp.src(['../asyluminfo/*', '../asyluminfo/**/*'], { base: '../asyluminfo', buffer: false } )
    .pipe( conn.newer(ftpDir) ) // only upload newer files
    .pipe( conn.dest(ftpDir) );
});

