<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
<title><?php wp_title( '-', true, 'right' ); ?></title>
<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
  <div id="page" class="hfeed site">
    <div id="wrap-header" class="wrap-header">
      <header id="masthead" class="site-header">
        <div class="site-branding">
          <h1 class="site-title">
              <a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
                 <!--<span>Asylum</span> info-->
                 <img src="<?php echo get_stylesheet_directory_uri() ?>/imgs/header.png" alt="Asylum Info">
                 <?php
                //    bloginfo( 'name' );
                 ?>
              </a>
            </h1>
          <h2 class="site-description"><?php bloginfo( 'description' ); ?></h2>
        </div>
      </header>
    </div>

    <?php
        // Simon: do not need navbar on front page
        if (!is_front_page()) : ?>
            <div class="wrap-navbar">
            <button id="responsive-menu-toggle"><?php _e( 'Menu', 'voidx' ); ?></button>
        <?php
            include('inc/navbar.php');
        ?>
            </div>
        <?php endif;
    ?>

    <div id="wrap-main" class="wrap-main">
