<?php

// [fpnav icon="live"]child content[/fpnav]

function fp_handler( $atts, $content ) {
    // set default values
    // $a = shortcode_atts( array(
    //     'foo' => 'something',
    //     'bar' => 'something else',
    // ), $atts );
    $img = get_stylesheet_directory_uri() . "/imgs/fp_" . $atts['icon'] . ".png";
    $returnValue = preg_match('/<span>([^<]*)<\/span>/', $content, $matches);
    $content = preg_replace('<br>', '', $content);

    $lang = pll_current_language();
// ‘$value’ => (optional) either ‘name’ or ‘locale’ or ‘slug’, defaults to ‘slug’
    return
        "<div class='fp-navigation'>
            <a href='./index.php/".$lang."/".strtolower($matches[1])."'>
                <img src='{$img}' >
                <h4>".$content."</h4>
            </a>
        </div>";
}
add_shortcode( 'fpnav', 'fp_handler' );