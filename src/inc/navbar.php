<?php
function menuImage($fname) {
    return '<img src="'.get_stylesheet_directory_uri().'/imgs/'.$fname.'.png">';
}
// getPolylangFamily();
?>

<nav id="site-navigation" class="site-navigation">
    <div id="responsive-menu">
    <?php wp_nav_menu(
        array(
            'theme_location' => 'header',
            'container_class' => 'menu-container',
            'menu_id' => 'menu-header',
            'menu_class' => 'menu-inline' ) );
            ?>
    </div>
</nav>

<?php
?>
