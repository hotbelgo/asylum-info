<?php

function spanFirstWord($content, $id = null) {
    if (is_page()) {
        // echo $content;
        preg_match("/([^\s]*)(.*)/", $content, $matches);
        // var_dump($matches);
        // preg_match("/(<[^>]*>)([^\s]*)(.*)/", $content, $matches);
        return "<span>".$matches[1]."</span>".$matches[2];
    }
    return $content;
}

add_filter( 'the_title', 'spanFirstWord', 10, 2 );

// prevent page class being added to homepage
function remove_a_body_class($wp_classes) {
    if( is_front_page() ) {
        foreach($wp_classes as $key => $value) {
           if ($value == 'page') unset($wp_classes[$key]);
        }
    }
    // var_dump($wp_classes);
    return $wp_classes;
}
add_filter('body_class', 'remove_a_body_class', 20, 2);


/* polylang */
function getPolylangFamily() {
    $ps = pll_the_languages(array(
        'post_id' => true
    ));
    var_dump($ps);
}
